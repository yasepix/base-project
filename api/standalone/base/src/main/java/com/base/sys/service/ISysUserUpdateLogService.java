package com.base.sys.service;

import com.base.sys.entity.SysUserUpdateLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-12-06
 */
public interface ISysUserUpdateLogService extends IService<SysUserUpdateLog> {

}
