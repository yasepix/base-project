package com.base.util;

import com.baomidou.mybatisplus.annotation.Version;
import com.sun.rmi.rmid.ExecOptionPermission;
import net.anumbrella.seaweedfs.core.FileSource;
import net.anumbrella.seaweedfs.core.FileTemplate;
import net.anumbrella.seaweedfs.core.file.FileHandleStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

@Component
@ConfigurationProperties(prefix = "seaweedfs")
public class Seaweedfs {

    private String host;

    private String port;

    private String timeout;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public FileSource startup() throws Exception{
        FileSource fileSource = null;
        fileSource = new FileSource();
        fileSource.setHost("120.79.5.163");
        fileSource.setPort(9333);
        fileSource.setConnectionTimeout(5000);
        fileSource.startup();
        return fileSource;
    }

    public String uploadFile(FileSource fileSource, String fileName, MultipartFile file) throws Exception {
        FileTemplate template = new FileTemplate(fileSource.getConnection());
        FileHandleStatus fileHandleStatus = template.saveFileByStream(fileName,file.getInputStream());
        String fileId = fileHandleStatus.getFileId();
        return fileId;
    }

    public void deleteFile(FileSource fileSource,String fileId)throws Exception{
        FileTemplate template = new FileTemplate(fileSource.getConnection());
        template.deleteFile(fileId);
    }


}
