package com.base.exception;

public class MyException extends RuntimeException{

    public MyException(String message) {
        super(message);
    }
}
