package com.base.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.sys.dto.SysUserQueryDTO;
import com.base.sys.dto.UserMenuPermissionDTO;
import com.base.sys.entity.SysUser;
import com.base.sys.entity.SysUserRole;
import com.base.sys.mapper.SysUserMapper;
import com.base.sys.mapper.SysUserRoleMapper;
import com.base.sys.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gjj
 * @since 2018-11-23
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public UserMenuPermissionDTO getUserMenuPermission(String id){
        return sysUserMapper.getUserMenuPermission(id);
    }


    @Override
    public IPage<SysUser> selectPageNew(Page page, Wrapper wrapper){
        Page<SysUser> sysUserPage = sysUserMapper.selectPageNew(page, wrapper);
        return sysUserPage;
    }

    @Override
    public List<SysUser> selectNew(Wrapper wrapper){
        List<SysUser> sysUserPage = sysUserMapper.selectPageNew( wrapper);
        return sysUserPage;
    }


    @Transactional
    @Override
    public Result assignRole(SysUser webUser){
        String[] roleIds = webUser.getRoleIds();
        QueryWrapper<SysUserRole> wrapper=new QueryWrapper<SysUserRole>();
        wrapper.eq("USER_ID",webUser.getId());
        sysUserRoleMapper.delete(wrapper);
        for(String roleId:roleIds){
            SysUserRole webUserRole=new SysUserRole();
            webUserRole.setRoleId(roleId);
            webUserRole.setUserId(webUser.getId());
            sysUserRoleMapper.insert(webUserRole);
        }
        return Result.successResult();
    }


}
