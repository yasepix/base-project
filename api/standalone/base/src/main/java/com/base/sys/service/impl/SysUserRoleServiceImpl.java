package com.base.sys.service.impl;

import com.base.sys.entity.SysUserRole;
import com.base.sys.mapper.SysUserRoleMapper;
import com.base.sys.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
